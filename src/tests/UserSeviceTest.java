package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import pl.edu.pjwstk.mpr.lab5.domain.Address;
import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;
import pl.edu.pjwstk.mpr.lab5.service.UserService;

public class UserSeviceTest {

	@Test
	public void testFindUsersWhoHaveMoreThanOneAddress() {
		
		Person p = new Person();
		Person p2 = new Person();
		
		User u = new User("name", "Passwd", p);
		User userWith2Addresses = new User("name2", "Passwd2", p2);
		
		Address a1 = new Address("Poland", "Gdynia", "Morska", 127);
		Address a2 = new Address("Poland", "Gdynia", "Ikara", 15);
		
		p.setAddresses(Arrays.asList(a1));
		p2.setAddresses(Arrays.asList(a1,a2));
		
		u.setPersonDetails(p);
		userWith2Addresses.setPersonDetails(p2);
		
		List<User> users = new ArrayList<User>();
		users.add(u);
		users.add(userWith2Addresses);

		List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);

		assertSame(result.get(0), userWith2Addresses); 
		
	}

	@Test
	public void testFindOldestPerson() {
		List<User> users;

		Person kamil = new Person();
		Person maciek = new Person();
		Person stefan = new Person();
		kamil.setAge(21);
		maciek.setAge(20);
		stefan.setAge(82);
		stefan.setName("Stefan");
		stefan.setSurname("Batory");
		
		User user1 = new User("saviola", "passwd", kamil);
		User user2 = new User("lesils", "passwd1", maciek);
		User user3 = new User("stefan", "pass123", stefan);

		users = Arrays.asList(user1, user2, user3);
		
		Person result = UserService.findOldestPerson(users);
		assertSame(82, result.getAge());
		
	}

	@Test
	public void testFindUserWithLongestUsername() {
		List<User> users;

		Person kamil = new Person();
		Person maciek = new Person();
		Person stefan = new Person();
		kamil.setAge(21);
		maciek.setAge(20);
		stefan.setAge(82);
		stefan.setName("Stefan");
		stefan.setSurname("Batory");
		
		User user1 = new User("saviola", "passwd", kamil);
		User user2 = new User("lesils", "passwd1", maciek);
		User user3 = new User("stefan", "pass123", stefan);

		users = Arrays.asList(user1, user2, user3);
		User result = UserService.findUserWithLongestUsername(users);
		assertSame("saviola", result.getName());
	}

	@Test
	public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() {
		
		List<User> users;
		
		Person kamil = new Person();
		Person maciek = new Person();
		Person stefan = new Person();
		
		kamil.setAge(21);
		kamil.setName("Kamil");
		kamil.setSurname("Nowak");
		
		maciek.setAge(20);
		maciek.setName("Maciek");
		maciek.setSurname("Konkol");
		
		stefan.setAge(82);
		stefan.setName("Stefan");
		stefan.setSurname("Batory");
		
		User user1 = new User("saviola", "passwd", kamil);
		User user2 = new User("lesils", "passwd1", maciek);
		User user3 = new User("stefan", "pass123", stefan);
		
		users = Arrays.asList(user1, user2, user3);
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		
		assertEquals("Kamil Nowak,Maciek Konkol,Stefan Batory", result);
	}
	

	@Test
	public void testGetSortedPermissionsOfUsersWithNameStartingWithA() {
		
		Person kamil = new Person();
		Person maciek = new Person();
		
		kamil.setAge(21);
		kamil.setName("Aamil");
		kamil.setSurname("Nowak");
		
		maciek.setAge(20);
		maciek.setName("Maciek");
		maciek.setSurname("Konkol");
		
		User user1 = new User("saviola", "passwd", kamil);
		User user2 = new User("lesils", "passwd1", maciek);
		
		List<User> users = Arrays.asList(user1, user2);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		kamil.setRole(roleGuest);
		maciek.setRole(roleAdmin);
		
		Permission permWrite = new Permission();
		Permission permRead = new Permission();
		Permission permExecute = new Permission();
		
		permWrite.setName("write");
		permRead.setName("read");
		permExecute.setName("execute");
		
		List<Permission> permGuest = Arrays.asList(permRead);
		List<Permission> permAdmin = Arrays.asList(permWrite,permRead,permExecute);
		
		roleGuest.setPermissions(permGuest);
		roleAdmin.setPermissions(permAdmin);
		
		assertEquals(Arrays.asList("read"),UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users));
		
	}

	@Test
	public void testGroupUsersByRole() {
		List<User> users;
		
		Person kamil = new Person();
		Person maciek = new Person();
		
		kamil.setAge(21);
		kamil.setName("Kamil");
		kamil.setSurname("Nowak");
		
		maciek.setAge(20);
		maciek.setName("Maciek");
		maciek.setSurname("Konkol");
		
	
		User user1 = new User("saviola", "passwd", kamil);
		User user2 = new User("lesils", "passwd1", maciek);
		List<User> guestList = Arrays.asList(user1);
		List<User> adminList = Arrays.asList(user2);
		users = Arrays.asList(user1, user2);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		kamil.setRole(roleGuest);
		maciek.setRole(roleAdmin);
		
		Map<Role, List<User>> result = UserService.groupUsersByRole(users);
		Map<Role, List<User>> test = new HashMap<Role, List<User>>();
		test.put(roleGuest, guestList );
		test.put(roleAdmin, adminList);
		assertEquals(test,result);
	}
	

	@Test
	public void testPartitionUserByUnderAndOver18() {
List<User> users;
		
		Person kamil = new Person();
		Person maciek = new Person();
		
		kamil.setAge(17);
		kamil.setName("Kamil");
		kamil.setSurname("Nowak");
		
		maciek.setAge(19);
		maciek.setName("Maciek");
		maciek.setSurname("Konkol");
		
	
		User user1 = new User("saviola", "passwd", kamil);
		User user2 = new User("lesils", "passwd1", maciek);
		List<User> under18List = Arrays.asList(user1);
		List<User> over18List = Arrays.asList(user2);
		users = Arrays.asList(user1, user2);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		kamil.setRole(roleGuest);
		maciek.setRole(roleAdmin);
		
		Map<Boolean, List<User>> result = UserService.partitionUserByUnderAndOver18(users);
		Map<Boolean, List<User>> test = new HashMap<Boolean, List<User>>();
		test.put(false, under18List );
		test.put(true, over18List);
		assertEquals(test,result);
	}

}
