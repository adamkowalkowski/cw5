
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import pl.edu.pjwstk.mpr.lab5.domain.Address;
import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;
import pl.edu.pjwstk.mpr.lab5.service.UserService;


public class Main {
	private static List<User> users;

	private static void init() {
		
		Person kamil = new Person();
		Person maciek = new Person();
		Person stefan = new Person();
		Person andrzej = new Person();
		Person agnieszka = new Person();
		
		kamil.setAge(21);
		kamil.setName("Aamil");
		kamil.setSurname("Sowak");
		
		maciek.setAge(20);
		maciek.setName("Dciek");
		maciek.setSurname("onkol");
		
		stefan.setAge(82);
		stefan.setName("Stefan");
		stefan.setSurname("datory");
		
		andrzej.setAge(24);
		andrzej.setName("dndrzej");
		andrzej.setSurname("canusz");
		
		agnieszka.setAge(22);
		agnieszka.setName("Anieszka");
		agnieszka.setSurname("sowak");
		
		User user1 = new User("saviola", "passwd", kamil);
		User user2 = new User("lesils", "passwd1", maciek);
		User user3 = new User("stefan", "pass123", stefan);
		User user4 = new User("endrju", "pass123", andrzej);
		User user5 = new User("aga", "pass123", agnieszka);
		
		users = Arrays.asList(user1, user2, user3,user4,user5);
		
		Role roleUser = new Role();
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		stefan.setRole(roleAdmin);
		kamil.setRole(roleGuest);
		agnieszka.setRole(roleUser);
		andrzej.setRole(roleGuest);
		maciek.setRole(roleAdmin);
		
		Address a1 = new Address("Poland", "Gdynia", "Morska", 127);
		Address a2 = new Address("Poland", "Gdynia", "Ikara", 15);
		Address a3 = new Address("Poland", "Gdynia", "Dedala", 27);
		
		stefan.setAddresses(Arrays.asList(a1,a2,a3));
		kamil.setAddresses(Arrays.asList(a1,a2));
		agnieszka.setAddresses(Arrays.asList(a1));
		andrzej.setAddresses(Arrays.asList(a1));
		maciek.setAddresses(Arrays.asList(a1));
		
		Permission permWrite = new Permission();
		Permission permRead = new Permission();
		Permission permExecute = new Permission();
		
		permWrite.setName("write");
		permRead.setName("read");
		permExecute.setName("execute");
		
		List<Permission> permUser = Arrays.asList(permWrite,permRead);
		List<Permission> permGuest = Arrays.asList(permRead);
		List<Permission> permAdmin = Arrays.asList(permWrite,permRead,permExecute);
		
		roleUser.setPermissions(permUser);
		roleGuest.setPermissions(permGuest);
		roleAdmin.setPermissions(permAdmin);
		List<User> moreThan2Addresses = UserService.findUsersWhoHaveMoreThanOneAddress(users);
		System.out.println(moreThan2Addresses);
		
		Person oldest = UserService.findOldestPerson(users);
		System.out.println("Najstarszy jest: " + oldest.getName() + " " 
							+ oldest.getSurname() + " lat " + oldest.getAge());
		User longest = UserService.findUserWithLongestUsername(users);
		System.out.println("Najdluzszy login ma: " + longest.getName());
		
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		System.out.println("Ukonczone 18 lat ma:" + result);
		
		List<String> nameStartingWithA = UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users);
		System.out.println("Posortowana lista uprawnien uzytkownikow z imieniem na A: ");
		System.out.println(nameStartingWithA);

		System.out.println("Lista uprawnien uzytkownikow z nazwiskiem na S: ");
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
		
		Map<Role, List<User>> groupUsersByRole = UserService.groupUsersByRole(users);
		System.out.println(groupUsersByRole);
		
		Map<Boolean, List<User>> groupUsersBy18 = UserService.partitionUserByUnderAndOver18(users);
		System.out.println(groupUsersBy18);
	
	}
	


	public static void main(String[] args) {
		init();
	
	}
}
