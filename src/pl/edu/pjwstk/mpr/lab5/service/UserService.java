package pl.edu.pjwstk.mpr.lab5.service;

import pl.edu.pjwstk.mpr.lab5.domain.Permission;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;




public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
    	
    	users = users.stream()
    	    	.filter(user -> user.getPersonDetails().getAddresses().size()>1)
    	    	.collect(Collectors.toList());
    	    	return users;
		
    }

    public static Person findOldestPerson(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
    	User oldest = users.stream()
    	        .max(Comparator.comparing(user -> user.getPersonDetails().getAge()))
    	        .get();
		return oldest.getPersonDetails();
        
    }

    public static User findUserWithLongestUsername(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
    	User longest = users.stream()
    	        .max(Comparator.comparing(user -> user.getName().length()))
    	        .get();
		return longest;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
    	return users.stream()
		.filter(user -> user.getPersonDetails().getAge() > 18)
		.map(user -> user.getPersonDetails().getName() + " " +
			user.getPersonDetails().getSurname()) 				
		.collect(Collectors.joining(","));
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
    	
        List<List<Permission>> listOfListsPermissions = users.stream()
                .filter(user -> user.getPersonDetails().getName().substring(0, 1).equalsIgnoreCase("A"))
                .map(user -> user.getPersonDetails().getRole().getPermissions())
                .collect(Collectors.toList());
                       
        List<Permission> listOfPermissions =
                    listOfListsPermissions.stream()
                        .flatMap(List::stream)
                        .collect(Collectors.toList());
           
        return listOfPermissions.stream()
                        .map(Permission::getName)
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList());  
		    }
    
    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
    	if(users==null)
			throw new NullPointerException();
    	
    	 List<List<Permission>> listOfListsPermissions = users.stream()
    	.filter(user -> user.getPersonDetails().getSurname().substring(0, 1).equalsIgnoreCase("S"))
    	.map(user-> user.getPersonDetails().getRole().getPermissions())
    	.collect(Collectors.toList());
    	
    	 List<Permission> listOfPermissions =
                 listOfListsPermissions.stream()
                     .flatMap(List::stream)
                     .collect(Collectors.toList());
    	 
    	 listOfPermissions.stream()
    	 		.map(Permission::getName)
    	 		.map(String::toUpperCase)
	 			.distinct()
	 			.sorted()
	 			.forEach(System.out::println);  
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
    	Map<Role, List<User>> groupUsersByRole = users.stream()
				.collect(Collectors.groupingBy(user-> user.getPersonDetails().getRole()));
		return groupUsersByRole;
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
    	Map<Boolean, List<User>> partitionUsersByAge = users.stream()
				.collect(Collectors.partitioningBy(user -> user.getPersonDetails().getAge() >= 18));
		return partitionUsersByAge;
    }
}
